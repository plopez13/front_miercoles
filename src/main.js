


import { createApp } from 'vue'
import App from './App.vue'

import ErrorVue from './pages/ErrorVue.vue'
import HomeVue from './pages/HomeVue.vue'
import CatalogVue from './pages/CatalogVue.vue'
import ListVue from './pages/ListVue.vue'
import ContactUs from './components/ContactUs.vue'
import ProductAbout from './pages/ProductAbout.vue'

import { createRouter, createWebHashHistory } from 'vue-router'

const routes = [
    {
        path:"/", component: HomeVue
    },
    {
        path:"/catalog", component: CatalogVue
    },
    {
        path:"/list", component: ListVue
    },
    {
        path:"/contact", component: ContactUs
    },
    {
        path:"/about", component: ProductAbout
    },
    {
        path: '/:pathMatch(.*)*', component: ErrorVue
    },
]

const router = createRouter({
    history: createWebHashHistory(),
    routes,
  });
const app = createApp(App).use(router);

app.use(router);

app.mount("#app");

